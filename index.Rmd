--- 
title: "A labour market network reveals clusters within regional manufacturing sector"
author: "Fabio Morea"
date: "`r Sys.Date()`"

documentclass: scrbook 
classoption: openany   #avoid white pages between chapters

output:
  bookdown::pdf_book:
    template: null

site: bookdown::bookdown_site
 
delete_merged_file: true

bibliography: [ref-books.bib, ref-packages.bib]
biblio-style: apalike
link-citations: yes
github-repo: fabiomorea/comp-emp
description: "R project containing core analysis and figures for a paper on community detection"
---

# Scope
This R project contains core analysis of the paper **Clusters of companies through communities of qualified employees**.





 