---
output:
  pdf_document: default
  html_document: default
---
# Data Preparation and explorarion  

This notebook is focused on preparation and exporation of data on companies and innovation activities
  

## basic libraries

```{r , include=FALSE}
# reproducible layout
set.seed(42)

# load libraries
library (tidyverse)
library (lubridate)
library (kableExtra)#tables
library (xtable)#tables
 
 
```



```{r echo=FALSE, message=FALSE, warning=FALSE}
companies <- read_csv("./../data_companies/cmp.csv")
sectors   <- read_csv("./../data_companies/nace.csv")
rating    <- read_csv("./../data_companies/rating.csv")
filter    <- read_delim("./../data_companies/nace_filter/NACE.csv", delim=";")
coords    <- read_csv("./../data_companies/impresa_indirizzo_coordinate.csv")%>%
  select(cf, sede_ul,long,lat)%>% filter(sede_ul=="SEDE")

```

```{r}
companies <- companies %>%
  left_join(coords, by="cf")
```


A  selection is based on **NACE codes** (further information in the Annexes).The selected sample is composed of companies that have at least one NACE code in one of the following Divisions: 22 (Manufacture of rubber and plastic products), 23 (Manufacture of other non-metallic mineral products), 24 (Manufacture of basic metals), 25 (Manufacture of fabricated metal products, except machinery and equipment), 26 (Manufacture of computer, electronic and optical products), 27 (Manufacture of electrical equipment) and 28 (Manufacture of machinery and equipment).


```{r}
#select 10 to 33 manufacturing C
# https://ec.europa.eu/eurostat/documents/3859598/5902521/KS-RA-07-015-EN.PDF.pdf/dd5443f5-b886-40e4-920d-9df03590ff91?t=1414781457000

#https://www.openriskmanual.org/wiki/NACE_Section_C_-_Manufacturing

sectors%>%
  filter(division %in% filter$division)%>%
  filter(loc_n==0) %>%
  filter(code_type!="S")%>%
  arrange(desc(code_type))%>%
  group_by(idCompany) %>%
  filter(row_number()==1) -> sectors

sectors%>%
  select(idCompany)%>%
  pull()%>%unique()->selected_companies

rating %>%
   mutate(cf = paste0("CF_",cf))%>%
   filter(year==2020)%>%
   select(cf, rating010)%>%
   rename(CF=cf)->rating

companies%>%
   rename(CF=cf)%>%
   mutate(CF = paste0("CF_",CF))%>%
   left_join(sectors)%>%
   left_join(rating,by="CF")%>%
   filter(prov %in% c("GO","TS","UD","PN"))%>%
   mutate(yearsInBusiness=round(yearsInBusiness,1))%>%
   mutate(is.innov = is.sme+is.startup)%>%
   select(idCompany,name,CF,prov,long,lat,ng2,yearsInBusiness,
           is.innov,is.sme,is.startup,is.young,is.fem, division,code, rating010) %>%
  distinct()->companies

companies%>%write_csv("comp_attributes.csv")

```



```{r}
#export as a filter for next steps
companies%>%
  select(CF,idCompany,division)%>%
  filter(idCompany%in%selected_companies)%>%
  write_csv("filter_nace_C.csv")

 
```




Gini Index uses the probability of finding a data point with one label as an indicator for homogeneity. If the dataset is completely homogeneous, then the probability of finding a datapoint with one of the labels is 1 and the probability of finding a data point with the other label is zero.

```{r}
gini_index <- function(x){
  f <- table(x)/length(x)
  g <-sum(f^2)
  print(paste("Gini index = ",g))
  return(g)
}

```

Summary by sector
```{r}
sect_summary <- companies %>% 
  select(idCompany, division) %>%
  mutate(division=as.numeric(division))%>%
  left_join(filter,by="division")%>%
  mutate(nace_division=paste(division,description_short))%>%
  select(idCompany,nace_division)%>%
  count(nace_division)%>% 
  mutate(perc=round(n/sum(n)*100,2))%>%
  arrange(desc(n))

  print(xtable(sect_summary),  
        type = "latex", include.rownames=FALSE,
        file="tables/table_sectors.tex")
  
  knitr::kable(sect_summary)
  
  gini_sector<-gini_index(sectors$division)

```

 
 Summary by province

```{r}

prov_summary <- companies %>% 
  select(idCompany, prov) %>%
  count( prov)%>% 
  mutate(perc=round(n/sum(n)*100,2))%>%

  arrange(desc(n))

knitr::kable(prov_summary)

print(xtable(prov_summary), 
        type = "latex", include.rownames=FALSE,
        file="tables/table_prov.tex")

gini_prov<-gini_index(companies$prov)

```
TODO: summarise gini index in a plot or table


```{r}

innov_summary <- companies %>% 
  select(idCompany, is.innov) %>%
   count( is.innov)%>% 
  mutate(perc=round(n/sum(n)*100,2))%>%
  arrange(desc(n))

knitr::kable(innov_summary)

print(xtable(innov_summary), 
        type = "latex", include.rownames=FALSE,
        file="tables/table_innov.tex")

gini_inno<-gini_index(companies$is.innov)

```

```{r}
# ratings of non-cap are NA, must be removed before summary and Gini
rating_summary <- companies %>% 
  select(idCompany, rating010) %>%
  filter(rating010>=1)

gini_rating<-gini_index(rating_summary$rating010)

rating_n<-rating_list%>%nrow()

rating_summary <-  rating_list%>%
  count( rating010)%>% 
  mutate(perc=round(n/rating_n*100,2))%>%
  arrange(desc(rating010))

knitr::kable(rating_summary)

ggplot(data=rating_summary, aes(x=factor(rating010),y=perc))+
  geom_bar(stat = "identity")

print(xtable(rating_summary), 
        type = "latex", include.rownames=FALSE,
        file="tables/table_rating.tex")


```

```{r}
#summary of Gini Index:
ginis<- c(gini_sector,gini_prov,gini_inno, gini_rating)
ginis

```
  
 ## TODO: fare grafico 
 
 ## TODO
 aggiungere (con left_join) informazioni su
 - brevetti
 - cordis
 - spinoff NETVAL
 
 